// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

// function flatten(elements) {
// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }

// create function
function flatten(elements) {
  //    create empty array
  const flattenedArray = [];

  // create function for recursive call
  function recursiveFlattern(arr) {
    for (let index = 0; index < arr.length; index++) {
      if (Array.isArray(arr[index])) {
        recursiveFlattern(arr[index]);
      } else {
        flattenedArray.push(arr[index]);
      }
    }
  }
  recursiveFlattern(elements);
  return flattenedArray;
}

module.exports = flatten;
