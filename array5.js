// function filter(elements, cb) {
// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
// }

// create function with two parameter
function filter(elements, cb) {
  // create empty array
  const filterElements = [];

  // use for loop to iterate and push elements
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      filterElements.push(elements[index]);
    }
  }
  return filterElements;
}

module.exports = filter;
