// import function
const filter = require("../array5");
// test array
const items = [1, 2, 3, 4, 5, 5];

// call function
const evenNumbers = filter(items, function (element) {
  return element % 2 === 0;
});

console.log(evenNumbers);
