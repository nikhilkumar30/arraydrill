// import the function
const find = require("../array4");
// test array
const items = [1, 2, 3, 4, 5, 5];
// call function and create call by function inside
const result = find(items, function cb(elements) {
  return elements === 3;
});

console.log(result);
