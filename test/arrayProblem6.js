// import function
const flatten = require("../array6");

// test array
const nestedArray = [1, [2], [[3]], [[[4]]]];

// call function
const result = flatten(nestedArray);

console.log(result);
