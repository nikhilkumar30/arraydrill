// import function
const each = require("../array1");

// test array
const items = [1, 2, 3, 4, 5, 5];

// call function
each(items, function (elements, index) {
  console.log(`Element at index ${index}: ${elements}`);
});
